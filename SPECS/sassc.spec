%global testspec_version 3.4.1

Name:           sassc
Version:        3.4.1
Release:        2%{?dist}
Summary:        Wrapper around libsass to compile CSS stylesheet

License:        MIT
URL:            http://github.com/sass/sassc
Source0:        https://github.com/sass/sassc/archive/%{version}.tar.gz
# Test suite spec. According to this comment from an upstream dev, we should
# not use the release tags on the test spec:
# https://github.com/sass/libsass/issues/2258#issuecomment-268196004
# https://github.com/sass/sass-spec/archive/master.zip
# https://github.com/sass/sass-spec/archive/v%{testspec_version}.tar.gz
Source1:        sass-spec-%{testspec_version}.tar.gz

# libsass is built as a shared library.
Patch0:         %{name}-3.3.6-build.patch

BuildRequires:  libsass-devel >= 3.4.0
# For the test suite
BuildRequires:  ruby
%if 0%{?epel} && 0%{?epel} <= 7
BuildRequires:  rubygem-minitest5
%else
BuildRequires:  rubygem-minitest
%endif

%description
SassC is a wrapper around libsass used to generate a useful command-line
application that can be installed and packaged for several operating systems.


%prep
%setup -q -a 1
mv sass-spec-%{testspec_version} sass-spec
%patch0 -p0 -b .build


%build
%make_build build-shared \
    LDFLAGS="$RPM_OPT_FLAGS" \
    CFLAGS="$RPM_OPT_FLAGS" \
    CXXFLAGS="$RPM_OPT_FLAGS"


%install
rm -rf $RPM_BUILD_ROOT
install -p -m755 -D bin/sassc $RPM_BUILD_ROOT%{_bindir}/%{name}


%check
ruby sass-spec/sass-spec.rb -V 3.4 -c bin/%{name} --impl libsass sass-spec/spec


%files
%license LICENSE
%doc Readme.md
%{_bindir}/%{name}


%changelog
* Sat Feb 11 2017 Fedora Release Engineering <releng@fedoraproject.org> - 3.4.1-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_26_Mass_Rebuild

* Mon Dec 19 2016 Aurelien Bompard <abompard@fedoraproject.org> - 3.4.1-1
- Version 3.4.1: https://github.com/sass/sassc/releases/tag/3.4.1

* Mon Dec 12 2016 Aurelien Bompard <abompard@fedoraproject.org> - 3.4.0-1
- Version 3.4.0: https://github.com/sass/sassc/releases/tag/3.4.0

* Tue Aug 23 2016 Aurelien Bompard <abompard@fedoraproject.org> - 3.3.6-1
- initial package
